#lang racket
;;
;; Higgins - A Scheme interface to Jenkins
;;
;The MIT License (MIT)
;
;Copyright (c) 2016 Cedric Nelson
;
;Permission is hereby granted, free of charge, to any person obtaining a copy
;of this software and associated documentation files (the "Software"), to deal
;in the Software without restriction, including without limitation the rights
;to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
;copies of the Software, and to permit persons to whom the Software is
;furnished to do so, subject to the following conditions:
;
;The above copyright notice and this permission notice shall be included in all
;copies or substantial portions of the Software.
;
;THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;SOFTWARE.

(require json net/base64 net/http-client net/uri-codec sxml)

(provide jenkins)

(define (jenkins [jenkins-host "127.0.0.1"] [jenkins-port 8080] #:ssl? [use-ssl #f] #:user [user #f] #:pass [pass #f])
  (define jenkins-root "/")
  (define api-path "api/json")

  (define (make-auth-header user pass)
    (string-append "Authorization: Basic "
                   (bytes->string/utf-8
                     (base64-encode
                       (string->bytes/utf-8
                         (string-append user ":" pass))
                       #""))))

  (define auth-header (if (and user pass) (list (make-auth-header user pass)) '()))

  (define (bool->str bool)
    (if bool "true" "false"))

  ; Generic GET
  (define (get url)
    (http-sendrecv jenkins-host url #:port jenkins-port #:ssl? use-ssl #:headers auth-header))

  ; Generic POST
  (define (post url headers data)
    (http-sendrecv jenkins-host url
                   #:ssl? use-ssl
                   #:port jenkins-port
                   #:method "POST"
                   #:headers headers
                   #:data data))

  (define (response-ok? status headers response [codes '()])
    (let* ((default-codes '(200 201 202 302 303))
           (ok-codes (if (null? codes) default-codes codes))
           (status-string (bytes->string/utf-8 status)))
      (match-let (((list* http-ver code text) (string-split status-string)))
        (if (member (string->number code) ok-codes) #t #f))))

  ; Do something with http response
  (define (http-handler status headers response [codes '(200 201 202 302 303)])
    (if (response-ok? status headers response codes) #t (error (bytes->string/utf-8 status) (assoc "X-Error" (parse-headers headers)))))

  ; POST to path without data
  (define (post-path path)
    (let ((url (string-append jenkins-root
                              (string-join path "/"))))
      (let-values (((status headers response)
                    (post url auth-header "")))
                  (http-handler status headers response))))

  ; POST with some headers and data
  (define (post-data url headers data result-proc)
    (let ((default-headers (append auth-header
                                   (list "Content-Type: application/x-www-form-urlencoded"))))
      (let-values (((status headers response)
                    (post url
                          (if (null? headers) default-headers headers)
                          data)))
                  (result-proc status headers response))))
  ; Get a url and pass response through a procedure
  (define (get-t url p)
    (let-values (((status headers response) (get url)))
                (p response)))

  ; Fetch a url and return parsed json from it
  (define (fetch-j url)
    (get-t url read-json))

  (define (xml-tree text)
    (ssax:xml->sxml text '()))

  ; Fetch a url and return parsed xml from it
  (define (fetch-x url)
    (get-t url xml-tree))

  ; Return jenkins daemon info
  (define (info)
    (let ((url (string-append jenkins-root api-path)))
      (fetch-j url)))

  (define (split-header str)
    (string-split (bytes->string/utf-8 str) ": "))

  (define (parse-headers strings)
    (map (lambda (s)
           (let ((header (split-header s)))
             (if (= (length header) 1)
                 (cons (car header) "")
                 (cons (car header) (cadr header)))))
         strings))

  ; Return jenkins daemon version
  ; NOTE(cedric): Named 'jenkins-version to avoid name collision with Racket 'version procedure
  (define (jenkins-version)
    (let ((url (string-append jenkins-root api-path)))
      (let-values (((status headers response) (get url)))
                  (cdr (assoc "X-Jenkins" (parse-headers headers))))))

  ; Return view info
  (define (views)
    (hash-ref (info) 'views))

  ; Return job info
  (define (jobs)
    (hash-ref (info) 'jobs))

  ; Return list of job names
  (define (job-names)
    (map (lambda (j) (hash-ref j 'name)) (jobs)))

  ; Return a job's parsed xml config
  (define (job-config name)
    (let ((url (string-append jenkins-root
                              (string-join (list "job" name "config.xml")
                              "/"))))
      (fetch-x url)))

  ; Issue a safeRestart command to the jenkins server.
  ;
  ; This has the effect of allowing running jobs to finish, and queueing
  ; jobs that would have started after this point. The queue persists through restart.
  (define (safe-restart)
    (let ((url (string-append jenkins-root "safeRestart"))
          (form (alist->form-urlencoded (list (cons 'json "{}")
                                              (cons 'Submit "Yes")))))
      (post-data url '() form http-handler)))

  ; Return installed plugin info
  (define (installed-plugins)
    (let ((url (string-append jenkins-root "pluginManager/" api-path "?depth=2")))
      (fetch-j url)))

  ; Return available plugins, plugin update info
  ; TODO(cedric): Should I maybe cache this somewhere?
  (define (plugins)
    (let ((url (string-append jenkins-root "updateCenter/site/default/" api-path "?depth=2")))
      (fetch-j url)))

  ; Return all plugin hashes
  (define (all-plugins)
    (let* ((installed (hash-ref (installed-plugins) 'plugins))
           (other (plugins))
           (available (hash-ref other 'availables))
           (pending-update (hash-ref other 'updates)))
      (append installed available pending-update)))

  ; Return plugin name info as (short-name . long-name), from given plugin hash
  (define (plugin-name p)
    (cond ((and (hash-has-key? p 'shortName)  ; installed uses 'shortName & 'longName
                (hash-has-key? p 'longName))
           (cons (hash-ref p 'shortName) (hash-ref p 'longName)))
          ((and (hash-has-key? p 'name)       ; available/updates uses 'name & 'title
                (hash-has-key? p 'title))
           (cons (hash-ref p 'name) (hash-ref p 'title)))))

  ; Return (short-long-alist . long-short-alist)
  (define (index-plugins)
    (let* ((plugin-names (map plugin-name (all-plugins)))
           (short-long plugin-names)
           (long-short (map (lambda (x) (cons (cdr x) (car x))) plugin-names))
           )
      (cons short-long long-short)))

  ; For the given plugin name, return a pair of (short-name . long-name)
  (define (resolve-plugin-name name)
    (let* ((index (index-plugins))
           (short-match (assoc name (car index)))
           (long-match (assoc name (cdr index))))
      (cond (short-match
             short-match)
            (long-match
              (cons (cdr long-match) (car long-match))))))

  ; Return true if plugin hash matches the given short name
  (define (is-plugin p short-name)
    (cond ((and (hash-has-key? p 'shortName)
                (equal? (hash-ref p 'shortName) short-name))
           #t)
          ((and (hash-has-key? p 'name)
                 (equal? (hash-ref p 'name) short-name))
           #t)
          (else #f)))

  ; Return info for the given plugin, by its short or long name
  (define (plugin-info name)
    (let* ((resolved (resolve-plugin-name name))
           (short-name (car resolved))
           (long-name (cdr resolved)))
      (car (filter (lambda (p) (is-plugin p short-name)) (all-plugins)))))

  ; Get the version of the given plugin, by its short or long name
  ;
  ; Example: (plugin-version "Green Balls")
  ;          (plugin-version "greenballs")
  (define (plugin-version name)
      (hash-ref (plugin-info name) 'version))

  ; Return some json meant for accompanying a plugin install operation
  ;
  ; Example usage: (plugin-install-json "Green Balls")
  ; {"Green Balls": {"default": true}}
  (define (plugin-install-json name)
    (jsexpr->string (hash (string->symbol name) (hash 'default #t))))

  ; Install the given plugin, by its short or long name
  ;
  ; Example: (install-plugin "Green Balls")
  ;          (install-plugin "greenballs")
  ;
  ; TODO(cedric): Make restart of jenkins after install optional, to support
  ; plugins that require restarts in order to become usable.
  (define (install-plugin name)
    (let* ((url (string-append jenkins-root "pluginManager/install"))
           (resolved (resolve-plugin-name name))
           (short-name (car resolved))
           (long-name (cdr resolved))
           (json-data (plugin-install-json long-name))
           (form (alist->form-urlencoded (list (cons (string->symbol (string-append "plugin." short-name ".default")) "on")
                                               (cons 'dynamicLoad "Install without restart")
                                               (cons 'json json-data)))))
      (post-data url '() form http-handler)))

  ; Uninstall the given plugin by its short or long name
  ;
  ; Example: (uninstall-plugin "greenballs")
  ;          (uninstall-plugin "Green Balls")
  ;
  ; TODO(cedric): Make restart of jenkins after uninstall optional, to support
  ; bulk uninstalls
  (define (uninstall-plugin name)
    (let* ((short-name (car (resolve-plugin-name name)))
           (url (string-append jenkins-root (string-join (list "pluginManager" "plugin" short-name "doUninstall") "/")))
           (form (alist->form-urlencoded (list (cons 'json "{}")))))
      (post-data url '() form http-handler)
      (safe-restart)))

  ; Return sxml for a (name . default-value) pair
  ; that represents the pair as a jenkins job parameter.
  ; This is meant to be used when generating job arg sxml
  (define (arg->sxml arg)
      (let* ((name (car arg))
             (value (cdr arg))
             (param-type (if (boolean? value)
                             'hudson.model.BooleanParameterDefinition
                             'hudson.model.StringParameterDefinition))
             (value-field (cond ((null? value) '(defaultValue))
                                ((boolean? value) `(defaultValue ,(if value "true" "false")))
                                ((number? value) `(defaultValue ,(number->string value)))
                                (else `(defaultValue ,value)))))
        `(,param-type
           (name ,name)
           (description)
           ,value-field)))

  ; Return some sxml representing any job arguments from the given parameters.
  ; This is meant to be used when generating job config sxml
  (define (job-args-sxml params)
    (let ((args (assoc 'args params)))
      (if args
          `(properties
             (hudson.model.ParametersDefinitionProperty
               (parameterDefinitions
                  ,@(map arg->sxml (cadr args)))))
          '(properties))))

  ; Generate job config sxml from a set of parameters
  ;
  ; Example: (job-sxml '((command "printenv") (schedule "32 20 * * *")))
  (define (job-sxml params)
    (let* ((description (or (assoc 'description params)
                            ""))
           (disabled-value (assoc 'disabled params))
           (disabled (if (pair? disabled-value) (bool->str (cadr disabled-value)) "false"))
           (node-label (assoc 'node-label params))
           (label (if node-label (cadr node-label) ""))
           (use-label (if node-label "false" "true"))
           (args-sxml (job-args-sxml params)))
      `(*TOP* (*PI* xml "version='1.0' encoding='UTF-8'")
              (project
                (actions)
                (description ,description)
                (keepDependencies "false")
                ,args-sxml
                (scm (@ (class "hudson.scm.NullSCM")))
                (assignedNode ,label)
                (canRoam ,use-label)
                (disabled ,disabled)
                (blockBuildWhenDownstreamBuilding "false")
                (blockBuildWhenUpstreamBuilding "false")
                (triggers
                  (hudson.triggers.TimerTrigger
                    (spec ,(cadr (assoc 'schedule params)))))
                (concurrentBuild "false")
                (builders
                  (hudson.tasks.Shell
                    (command ,(cadr (assoc 'command params)))))
                (publishers)
                (buildWrappers)))))

  ; Create job
  ;
  ; params -> should be a association list of (parameter-name . value) pairs
  ; Required params:
  ;   schedule        ->  string  (jenkins-style cron schedule)
  ;   command         ->  string
  ; Optional params:
  ;   description     ->  string
  ;   disabled        ->  bool
  ;   node-label      ->  string  (restrict which nodes job can run on)
  ;   args            ->  alist   (list of (name . default-value) pairs.
  ;                                If default-value is '(), then it isn't populated
  ;                                as a field in jenkins job config)
  ;
  ; Example: (create-job "banana" '((command "printenv") (schedule "32 20 * * *")))
  (define (create-job name params)
    (let ((url (string-append jenkins-root "createItem" "?name=" name))
          (headers (append auth-header (list "Content-Type: application/xml")))
          (sxml (job-sxml params)))
      (post-data url headers (srl:sxml->xml sxml) http-handler)))

  ; Update job, using sxml
  (define (update-job-sxml name sxml)
    (let ((url (string-append jenkins-root
                              (string-join (list "job" name "config.xml") "/")))
           (headers (append auth-header (list "Content-Type: application/xml"))))
      (post-data url headers (srl:sxml->xml sxml) http-handler)))

  ; Update job, using same parameters as create-job
  (define (update-job name params)
    (update-job-sxml name (job-sxml params)))

  ; Delete job
  (define (delete-job name)
    (post-path (list "job" name "doDelete")))

  ; Disable job
  (define (disable-job name)
    (post-path (list "job" name "disable")))

  ; Enable job
  (define (enable-job name)
    (post-path (list "job" name "enable")))

  ; Build job
  ;
  ; Example: (build-job "banana") ; Trigger build of "banana" without params
  ;          (build-job "banana" '((turtle . "kevin"))) ; Build with param turtle=kevin
  (define (build-job name [build-params '()])
    (let* ((default-params '((Submit . "Build")))
           (build-url (string-append jenkins-root
                                    (string-join (list "job" name "build")
                                                 "/")))
           (build-params-url (string-append build-url "WithParameters"))
           (selected-url (if (null? build-params) build-url build-params-url))
           (selected-headers (if (null? build-params)
                                 auth-header
                                 (append auth-header (list "Content-Type: application/x-www-form-urlencoded"))))
           (selected-params (if (null? build-params) default-params build-params))
           (form (alist->form-urlencoded selected-params)))
           (let-values (((status headers response)
                         (post selected-url selected-headers form)))
             (cdr (assoc "Location" (parse-headers headers))))))

  ; Copy job
  ;
  ; Example: (copy-job "banana" "tomato") ; Copy job "banana" to new job "tomato"
  (define (copy-job from to)
    (let ((url (string-append jenkins-root "createItem"))
          (form (alist->form-urlencoded `((mode . "copy")
                                          (from . ,from)
                                          (name . ,to)))))
      (post-data url '() form http-handler)))

  ; Return jenkins node info
  (define (nodes)
    (let ((url (string-append jenkins-root "computer/" api-path "?depth=2")))
      (fetch-j url)))

  ; Return a list of node names
  (define (node-names)
    (map (lambda (n) (hash-ref n 'displayName)) (hash-ref (nodes) 'computer)))

  ; Return node info
  (define (node name)
    (let ((url (string-append jenkins-root
                              (string-join (list "computer" name api-path)
                                           "/")
                              "?depth=2")))
      (fetch-j url)))

  ; Generate node creation alist from a set of parameters
  (define (node-alist name params)
    (let* ((defaults `((name . ,name)
                       (type . "hudson.slaves.DumbSlave")
                       (numExecutors . "1")
                       (labelString . "")
                       (mode . "EXCLUSIVE")
                       (host . "")
                       (port . "22")
                       (javaPath . "")
                       (jvmOptions . "")
                       (prefixStartSlaveCmd . "")
                       (suffixStartSlaveCmd . "")
                       (launchTimeoutSeconds . "")
                       (maxNumRetries . "")
                       (retryWaitTime . "")
                       ; NOTE(cedric): In the top-level stapler-class is SSHLauncher, but in
                       ; retentionStrategy the same variable is RetentionStrategy$Always
                       (stapler-class . "hudson.plugins.sshslaves.SSHLauncher")
                       ))
           (param-mapping '((description . nodeDescription)
                            (executors . numExecutors)
                            (node-root . remoteFS)
                            (node-label . labelString)
                            (credentials-id . credentialsId)))
           ; Replace parameter names with the ones that will be used when issuing
           ; node-creation web request
           (mapped (map (lambda (p) (let ((match (assoc (car p) param-mapping)))
                                      (if match
                                          (cons (cdr match) (cdr p))
                                          p))) params))
           ; Merge default values with mapped parameters, where any specified values
           ; overwrite the defaults
           (merged (append mapped defaults))
           (top '(name nodeDescription numExecutors remoteFS labelString mode type))
           (launcher-n '(stapler-class host port credentialsId javaPath jvmOptions
                         prefixStartSlaveCmd suffixStartSlaveCmd launchTimeoutSeconds
                         maxNumRetries retryWaitTime))
           ; Generate json string for node creation form payload
           (json-template (make-hash `(,@(map (lambda (n) (assoc n merged)) top) ; Populate top-level json values
                                       (launcher . ,(make-hash (map (lambda (n) (assoc n merged)) launcher-n)))
                                       (retentionStrategy . #hash((stapler-class . "hudson.slaves.RetentionStrategy$Always")))
                                       (nodeProperties . #hash((stapler-class-bag . #t))))))
           (json-data (jsexpr->string json-template)))
      `((name . ,name)
        (type . ,(cdr (assoc 'type merged)))
        (json . ,json-data))))

  ; Create node
  ;
  ; params -> should be a association list of (parameter-name . value) pairs
  ; Required params:
  ;   node-root       ->  string  (path of remote jenkins agent process's root dir)
  ;   credentials-id  ->  string  (id of credentials to use for ssh connection to host)
  ; Optional params:
  ;   host            ->  string  (fqdn/ip of remote jenkins node; default is localhost)
  ;   port            ->  string  (tcp port to use for ssh connection to host; default is 22)
  ;   description     ->  string
  ;   executors       ->  string  (number of simultaneous jobs node can run; default is 1)
  ;   node-label      ->  string  (restrict what jobs can run on this node)
  ;
  ; Example: (create-node "bloop" '((node-root . "/home/cedric")
  ;                                 (credentials-id . "f834adae-e594-4545-8286-b6a5933d6a94")
  ;                                 (host . "10.0.7.83")
  ;                                 (description . "AWS region=us-west-2 instance-id=12345")
  ;                                 (executors . "3")
  ;                                 (node-label . "low-priority")))
  ;
  ; TODO(cedric): This process is complex enough that we may want to error-out early if required params aren't defined
  ; TODO(cedric): Allow integers to be specified for certain parameters (port, executors, etc) and transform to strings
  (define (create-node name params)
    (let ((url (string-append jenkins-root "computer/doCreateItem")))
      (post-data url '() (alist->form-urlencoded (node-alist name params)) http-handler)))

  ; Delete node
  (define (delete-node name)
    (let ((url (string-append jenkins-root
                              (string-join (list "computer" name "doDelete")
                                           "/")))
          (form (alist->form-urlencoded '((json . "{}")
                                          (Submit . "Yes")))))
      (post-data url '() form http-handler)))

  ; Disable node
  (define (disable-node name)
    (let* ((url (string-append jenkins-root
                              (string-join (list "computer" name "toggleOffline")
                                           "/")))
           (message (cons 'offlineMessage "disabled by higgins"))
           (form (alist->form-urlencoded `(,message
                                           (json . ,(jsexpr->string (make-hash (list message))))
                                           (Submit . "Mark this node temporarily offline")))))
      (post-data url '() form http-handler)))

  ; Enable node
  (define (enable-node name)
    (let ((url (string-append jenkins-root
                              (string-join (list "computer" name "toggleOffline")
                                           "/")))
          (form (alist->form-urlencoded '((json . "{}")
                                          (Submit . "Bring this node back online")))))
      (post-data url '() form http-handler)))

  ; NOTE(cedric): Listing credential domain info (to get individual domain names)
  ; doesn't seem to work with Jenkins 1.651.2, via /credential-store/api/json?depth=2
  ;
  ; Because of this, only listing credential information from the default Global domain
  ; will work for now.

  ; Return credential info
  (define (credentials)
    (let ((url (string-append jenkins-root
                              (string-join (list "credential-store" "domain" "_" api-path)
                                           "/")
                              "?depth=2")))
      (fetch-j url)))

  ; Return (name . id) alist for credentials
  (define (credentials->alist)
    (hash-map (hash-ref (credentials) 'credentials)
              (lambda (id c)
                (cons (hash-ref c 'displayName) (symbol->string id)))))

  ; Return a list of credential names
  (define (credential-names)
    (map (lambda (c) (car c)) (credentials->alist)))

  ; Return credential id for the given credential name
  (define (credential-id name)
    (cdr (assoc name (credentials->alist))))

  ; Provide access to procedures in local environment
  (define (dispatch m . args)
    (cond ((equal? m 'info) (apply info args))
          ((equal? m 'version) (apply jenkins-version args))
          ((equal? m 'views) (apply views args))
          ((equal? m 'jobs) (apply jobs args))
          ((equal? m 'job-names) (apply job-names args))
          ((equal? m 'job-config) (apply job-config args))
          ((equal? m 'safe-restart) (apply safe-restart args))
          ((equal? m 'installed-plugins) (apply installed-plugins args))
          ((equal? m 'plugins) (apply plugins args))
          ((equal? m 'all-plugins) (apply all-plugins args))
          ((equal? m 'resolve-plugin-name) (apply resolve-plugin-name args))
          ((equal? m 'plugin-info) (apply plugin-info args))
          ((equal? m 'plugin-version) (apply plugin-version args))
          ((equal? m 'install-plugin) (apply install-plugin args))
          ((equal? m 'uninstall-plugin) (apply uninstall-plugin args))
          ((equal? m 'job-sxml) (apply job-sxml args))
          ((equal? m 'create-job) (apply create-job args))
          ((equal? m 'update-job-sxml) (apply update-job-sxml args))
          ((equal? m 'update-job) (apply update-job args))
          ((equal? m 'delete-job) (apply delete-job args))
          ((equal? m 'disable-job) (apply disable-job args))
          ((equal? m 'enable-job) (apply enable-job args))
          ((equal? m 'build-job) (apply build-job args))
          ((equal? m 'copy-job) (apply copy-job args))
          ((equal? m 'nodes) (apply nodes args))
          ((equal? m 'node-names) (apply node-names args))
          ((equal? m 'node) (apply node args))
          ((equal? m 'node-alist) (apply node-alist args))
          ((equal? m 'create-node) (apply create-node args))
          ((equal? m 'delete-node) (apply delete-node args))
          ((equal? m 'disable-node) (apply disable-node args))
          ((equal? m 'enable-node) (apply enable-node args))
          ((equal? m 'credentials) (apply credentials args))
          ((equal? m 'credentials->alist) (apply credentials->alist args))
          ((equal? m 'credential-names) (apply credential-names args))
          ((equal? m 'credential-id) (apply credential-id args))
          (else (error "Unknown message -- jenkins" m))))
  dispatch)

