#!/usr/bin/env racket
#lang racket/base

; NOTE(cedric): This test suite is not safe to run against a production jenkins instance
; (possible job name collisions, installing/uninstalling plugins)
; Be sure that you run this against a test instance of jenkins ^_^

(require rackunit "higgins.scm")
(require rackunit/text-ui)

(define j (jenkins "127.0.0.1" 8080))
;(define j (jenkins "127.0.0.1" 8990 #:ssl? #t))
;(define j (jenkins "127.0.0.1" 8990 #:ssl? #t #:user "cedric" #:pass "passwordhere"))

(define test-job-name "higgins-test-banana")
(define test-copied-job-name "higgins-test-lemon")
(define test-job-sxml '((command "printenv") (schedule "32 20 * * *")))
(define test-plugin "greenballs")
(define test-plugin-full "Green Balls")
(define test-node-name "higgins-test-bloop")
(define test-node-alist '((node-root . "/home/cedric")
                          (credentials-id . "f834adae-e594-4545-8286-b6a5933d6a94")
                          (host . "10.0.7.83")
                          (description . "AWS region=us-west-2 instance-id=12345")
                          (executors . "3")
                          (node-label . "low-priority")))

(define higgins-tests
  (test-suite
    "Tests for higgins.scm"
    (check-not-false (j 'info) "can't find jenkins info")
    (check-not-false (j 'version) "can't find jenkins version")
    (check-not-false (j 'views) "no views in jenkins info")
    (check-not-false (j 'jobs) "can't find jenkins jobs")
    (check-not-false (j 'job-names) "can't list jenkins job names")
    (check-not-false (j 'create-job test-job-name test-job-sxml) "job creation")
    (check-not-false (member test-job-name (j 'job-names)) "job creation confirmed")
    (check-not-false (j 'update-job test-job-name test-job-sxml) "job update")
    (check-not-false (j 'disable-job test-job-name) "disable job")
    (check-not-false (j 'enable-job test-job-name) "enable job")
    (check-not-false (j 'build-job test-job-name) "build job")
    (check-not-false (j 'copy-job test-job-name test-copied-job-name) "copy job")
    (check-not-false (j 'delete-job test-job-name) "job deletion")
    (check-not-false (j 'delete-job test-copied-job-name) "copied job deletion")
    (check-false (member test-job-name (j 'job-names)) "job deletion confirmed")

    (check-not-false (j 'nodes) "can't find node info")
    (check-not-false (j 'create-node test-node-name test-node-alist) "node creation")
    (check-not-false (member test-node-name (j 'node-names)) "node creation confirmed")
    (check-not-false (j 'disable-node test-node-name) "disable node")
    (check-not-false (j 'enable-node test-node-name) "enable node")
    (check-not-false (j 'delete-node test-node-name) "node deletion")

    (check-not-false (j 'credentials) "global credentials info")
    (check-not-false (j 'credentials->alist) "credentials info as alist")
    (check-not-false (j 'credential-names) "credential names")

    (check-not-false (j 'installed-plugins) "installed plugin info")
    (check-not-false (j 'plugins) "available/update plugin info")
    (check-not-false (j 'all-plugins) "all plugin info")
    (check-not-false (j 'plugin-info test-plugin))
    (check-true (string? (j 'plugin-version test-plugin)))
    (check-equal? (j 'resolve-plugin-name test-plugin) (cons test-plugin test-plugin-full) "short->long plugin resolve")
    (check-equal? (j 'resolve-plugin-name test-plugin-full) (cons test-plugin test-plugin-full) "long->short plugin resolve")
    (j 'install-plugin test-plugin)
    ; Allow for plugin to download/install
    (sleep 5)
    (j 'uninstall-plugin test-plugin) ; This (currently) triggers jenkins instance restart!
    ))

(run-tests higgins-tests)
